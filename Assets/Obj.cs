﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obj   : MonoBehaviour
{
	Vector3 vecTempPos = new Vector3();

	public float _dirx, _diry;

	public SpriteRenderer _srMain;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	public virtual void Update () {

		Ejecting ();

	}

	void Awake()
	{
		Init ();
	}

	public virtual void Init()
	{
		_srMain = this.gameObject.GetComponent<SpriteRenderer> ();
	}


	bool m_bIsEjecting = false;
	float m_fEjectInitialSpeed = 0.0f;
	float m_fEjectSpeed = 0.0f;
	float m_fEjectAccelerate = 0.0f;
	float m_fStartPosX = 0.0f;
	float m_fStartPosY = 0.0f;
	float m_fTimeClapse = 0.0f;
	public void BeginEject( float fInitialSpeed, float fAccelerate  )
	{
		m_bIsEjecting = true;
		m_fEjectInitialSpeed = fInitialSpeed;
		m_fEjectSpeed = fInitialSpeed;
		m_fEjectAccelerate = fAccelerate;

		// poppin to do
		/*
		if (_Collider) {
			_Collider.isTrigger = true;
		}
		*/

		m_fStartPosX = this.gameObject.transform.position.x;
		m_fStartPosY = this.gameObject.transform.position.y;
		m_fTimeClapse = 0.0f;
	}

	public void EndEject()
	{
		m_bIsEjecting = false;

		// poppin to do
		/*
		if (m_bShell) {
			if (_Collider) {
				_Collider.isTrigger = false;
			}
		}

		if (_relateTarget) {
			DestroyTarget ( _relateTarget );
		}
		*/
	}



	void Ejecting()
	{
		if (!m_bIsEjecting) {
			return;
		}

		if (m_fTimeClapse >= Main.s_Instance.m_fSpitRunTime) {
			EndEject ();
			return;
		}

		m_fTimeClapse += Time.deltaTime;
		vecTempPos.x = m_fStartPosX;
		vecTempPos.y = m_fStartPosY;
		vecTempPos.z = this.gameObject.transform.position.z;
		float S = CyberTreeMath.CalculateDistance( m_fEjectInitialSpeed, m_fEjectAccelerate, m_fTimeClapse ); //m_fEjectSpeed * m_fTimeClapse + 0.5f * m_fAccelerate * m_fTimeClapse * m_fTimeClapse;
		vecTempPos.x += S * _dirx;
		vecTempPos.y += S * _diry;
		this.gameObject.transform.position = vecTempPos;
	}

	public Vector3 GetPos()
	{
		return this.gameObject.transform.position;
	}

	public void SetPos( float fX, float fY )
	{
		vecTempPos = this.gameObject.transform.position;
		vecTempPos.x = fX;
		vecTempPos.y = fY;
		this.gameObject.transform.position = vecTempPos;
	}

	public void SetDir( float fX, float fY )
	{
		_dirx = fX;
		_diry = fY;
	}

}
