/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID PLAYBGM = 2542411811U;
        static const AkUniqueID STOPBGM = 3203837125U;
        static const AkUniqueID TRIGGEREXPLOSION = 267745220U;
        static const AkUniqueID TRIGGERSWIPE = 2868093727U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace PLAYLISTSTATESGROUP
        {
            static const AkUniqueID GROUP = 442355142U;

            namespace STATE
            {
                static const AkUniqueID STATE_00 = 3794091079U;
                static const AkUniqueID STATE_01 = 3794091078U;
                static const AkUniqueID STATE_02 = 3794091077U;
                static const AkUniqueID STATE_03 = 3794091076U;
            } // namespace STATE
        } // namespace PLAYLISTSTATESGROUP

    } // namespace STATES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID SS_AIR_FEAR = 1351367891U;
        static const AkUniqueID SS_AIR_FREEFALL = 3002758120U;
        static const AkUniqueID SS_AIR_FURY = 1029930033U;
        static const AkUniqueID SS_AIR_MONTH = 2648548617U;
        static const AkUniqueID SS_AIR_PRESENCE = 3847924954U;
        static const AkUniqueID SS_AIR_RPM = 822163944U;
        static const AkUniqueID SS_AIR_SIZE = 3074696722U;
        static const AkUniqueID SS_AIR_STORM = 3715662592U;
        static const AkUniqueID SS_AIR_TIMEOFDAY = 3203397129U;
        static const AkUniqueID SS_AIR_TURBULENCE = 4160247818U;
    } // namespace GAME_PARAMETERS

    namespace TRIGGERS
    {
        static const AkUniqueID EXPLOSIONTRIGGER = 1810158906U;
        static const AkUniqueID SWIPETRIGGER = 2590092299U;
    } // namespace TRIGGERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MAINSOUNDBANK = 534561221U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MASTER_SECONDARY_BUS = 805203703U;
    } // namespace BUSSES

}// namespace AK

#endif // __WWISE_IDS_H__
