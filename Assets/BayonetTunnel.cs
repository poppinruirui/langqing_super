﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BayonetTunnel : MonoBehaviour {

    List<Bayonet> m_lstBayonets = new List<Bayonet>();
    public GameObject m_goBayonetContainer;
    Bayonet m_CurMovingBayo = null;
    int m_nCurMovingBayoIndex = 0;

    // Use this for initialization
    void Start () {
		
	}

    void Awake()
    {
        Init();
    }
	
	// Update is called once per frame
	void Update () {
        Moving();
    }

    void Init()
    {
        foreach (Transform child in m_goBayonetContainer.transform)
        {
            Bayonet bayo = child.gameObject.GetComponent<Bayonet>();
            if (bayo == null)
            {
                continue;
            }
            bayo._bayoTunnel = this;
            m_lstBayonets.Add( bayo );
        }

        if (PhotonNetwork.isMasterClient)
        {
            BeginMove(0);
        }
    }

    void BeginMove( int nIndex )
    {
        if (nIndex >= m_lstBayonets.Count)
        {
            return;
        }
        Bayonet bayo = m_lstBayonets[nIndex];
        if (bayo == null)
        {
            return;
        }
        bayo.BeginMove();
        m_nCurMovingBayoIndex = nIndex;
        m_CurMovingBayo = bayo;
    }

    void Moving()
    {
        if (m_CurMovingBayo == null)
        {
            return;
        }

        if (!m_CurMovingBayo.IsMoving())
        {
            m_nCurMovingBayoIndex++;
            if (m_nCurMovingBayoIndex >= m_lstBayonets.Count)
            {
                m_nCurMovingBayoIndex = 0;
            }
            BeginMove(m_nCurMovingBayoIndex);
        }

    }
}
