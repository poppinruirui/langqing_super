using UnityEngine;

public class ColorPalette
{
    // background colors
    public static string[] _color_strs_bg = {       
        "#d50000", // red
        "#c51162", // pink
        "#aa00ff", // purple
        "#6200ea", // deep purple
        "#304ffe", // indigo
        "#2962ff", // blue
        "#0091ea", // light blue
        "#00b8d4", // cyan
        "#00bfa5", // teal
        "#00c853", // green
        "#64dd17", // light green
        "#aeea00", // lime
        "#ffd600", // yellow
        "#ffab00", // amber
        "#ff6d00", // orange
        "#dd2c00", // deep orange
        "#3e2723", // brown
        "#212121", // gray
        "#263238", // blue gray        
    };    
    public static Color[] _colors_bg = new Color[ColorPalette._color_strs_bg.Length];

    public static float[] _ranges_bg_light = {
        18, // red
        18, // pink
        20, // purple
        20, // deep purple
        20, // indigo
        20, // blue
        18, // light blue
        20, // cyan
        20, // teal
        20, // green
        18, // light green
        15, // lime
        13, // yellow
        20, // amber
        18, // orange
        18, // deep orange
        25, // brown
        25, // gray
        25, // blue gray
    };

    // ball ring colors
    public static string[] _color_strs_ring = {
        "#ffebee", // red
        "#fce4ec", // pink
        "#f3e5f5", // purple
        "#ede7f6", // deep purple
        "#e8eaf6", // indigo
        "#e3f2fd", // blue
        "#e1f5fe", // light blue
        "#e0f7fa", // cyan
        "#e0f2f1", // teal
        "#e8f5e9", // green
        "#f1f8e9", // light green
        "#f9fbe7", // lime
        "#fffde7", // yellow
        "#fff8e1", // amber
        "#fff3e0", // orange
        "#fbe9e7", // deep orange
        "#efebe9", // brown
        "#fafafa", // gray
        "#eceff1", // blue gray
    };
    public static Color[] _colors_ring = new Color[ColorPalette._color_strs_ring.Length];
    
    // ball inner colors
    public static string[] _color_strs_inner = {       
        "#b71c1c", // red
        "#880e4f", // pink
        "#4a148c", // purple
        "#311b92", // deep purple
        "#1a237e", // indigo
        "#0d47a1", // blue
        "#01579b", // light blue
        "#006064", // cyan
        "#004d40", // teal
        "#1b5e20", // green
        "#33691e", // light green
        "#827717", // lime
        "#f57f17", // yellow
        "#ff6f00", // amber
        "#e65100", // orange
        "#bf360c", // deep orange
        "#3e2723", // brown
        "#212121", // gray
        "#263238", // blue gray
    };
    public static Color[] _colors_inner = new Color[ColorPalette._color_strs_inner.Length];

    // ball poison colors
    public static string[] _color_strs_poison = {       
        "#ff5252", // red
        "#ff4081", // pink
        "#e040fb", // purple
        "#7c4dff", // deep purple
        "#536dfe", // indigo
        "#448aff", // blue
        "#40c4ff", // light blue
        "#18ffff", // cyan
        "#64ffda", // teal
        "#69f0ae", // green
        "#b2ff59", // light green
        "#eeff41", // lime
        "#ffff00", // yellow
        "#ffd740", // amber
        "#ffab40", // orange
        "#ff6e40", // deep orange
        "#4e342e", // brown
        "#424242", // gray
        "#37474f", // blue gray
    };    
    public static Color[] _colors_poison = new Color[ColorPalette._color_strs_poison.Length];

    // food colors
    public static string[] _color_strs_food = {       
        "#ff5252", // red
        "#ff4081", // pink
        "#e040fb", // purple
        "#7c4dff", // deep purple
        "#536dfe", // indigo
        "#448aff", // blue
        "#40c4ff", // light blue
        "#18ffff", // cyan
        "#64ffda", // teal
        "#69f0ae", // green
        "#b2ff59", // light green
        "#eeff41", // lime
        "#ffff00", // yellow
        "#ffd740", // amber
        "#ffab40", // orange
        "#ff6e40", // deep orange
        "#4e342e", // brown
        "#424242", // gray
        "#37474f", // blue gray
    };    
    public static Color[] _colors_food = new Color[ColorPalette._color_strs_food.Length];

    // hedge colors
    public static string[] _color_strs_hedge = {       
        "#ff5252", // red
        "#ff4081", // pink
        "#e040fb", // purple
        "#7c4dff", // deep purple
        "#536dfe", // indigo
        "#448aff", // blue
        "#40c4ff", // light blue
        "#18ffff", // cyan
        "#64ffda", // teal
        "#69f0ae", // green
        "#b2ff59", // light green
        "#eeff41", // lime
        "#ffff00", // yellow
        "#ffd740", // amber
        "#ffab40", // orange
        "#ff6e40", // deep orange
        "#4e342e", // brown
        "#424242", // gray
        "#37474f", // blue gray
    };    
    public static Color[] _colors_hedge = new Color[ColorPalette._color_strs_hedge.Length];    
        
    static ColorPalette()
    {
        for (int i=0; i<ColorPalette._color_strs_bg.Length; ++i) {
            Color color = Color.black;
            if (ColorUtility.TryParseHtmlString(ColorPalette._color_strs_bg[i],
                                                out color)) {
                ColorPalette._colors_bg[i] = color;
            }
            else {
                Debug.Log("invalid background color at: " + i +
                          " -> " + ColorPalette._color_strs_bg[i]);
            }
        }
        for (int i=0; i<ColorPalette._color_strs_ring.Length; ++i) {
            Color color = Color.black;
            if (ColorUtility.TryParseHtmlString(ColorPalette._color_strs_ring[i],
                                                out color)) {
                ColorPalette._colors_ring[i] = color;
            }
            else {
                Debug.Log("invalid background color at: " + i +
                          " -> " + ColorPalette._color_strs_ring[i]);
            }            
        }
        for (int i=0; i<ColorPalette._color_strs_inner.Length; ++i) {
            Color color = Color.black;
            if (ColorUtility.TryParseHtmlString(ColorPalette._color_strs_inner[i],
                                                out color)) {
                ColorPalette._colors_inner[i] = color;
            }
            else {
                Debug.Log("invalid background color at: " + i +
                          " -> " + ColorPalette._color_strs_inner[i]);
            }                 
        }
        for (int i=0; i<ColorPalette._color_strs_poison.Length; ++i) {
            Color color = Color.black;
            if (ColorUtility.TryParseHtmlString(ColorPalette._color_strs_poison[i],
                                                out color)) {
                ColorPalette._colors_poison[i] = color;
            }
            else {
                Debug.Log("invalid background color at: " + i +
                          " -> " + ColorPalette._color_strs_poison[i]);
            }                 
        }
        for (int i=0; i<ColorPalette._color_strs_food.Length; ++i) {
            Color color = Color.black;
            if (ColorUtility.TryParseHtmlString(ColorPalette._color_strs_food[i],
                                                out color)) {
                ColorPalette._colors_food[i] = color;
            }
            else {
                Debug.Log("invalid background color at: " + i +
                          " -> " + ColorPalette._color_strs_food[i]);
            }                 
        }
        for (int i=0; i<ColorPalette._color_strs_hedge.Length; ++i) {
            Color color = Color.black;
            if (ColorUtility.TryParseHtmlString(ColorPalette._color_strs_hedge[i],
                                                out color)) {
                ColorPalette._colors_hedge[i] = color;
            }
            else {
                Debug.Log("invalid background color at: " + i +
                          " -> " + ColorPalette._color_strs_hedge[i]);
            }
        }        
    }
    
    public static Color RandomBackgroundColor(out float light_range)
    {
            int color_index =
                Random.Range(0, ColorPalette._colors_bg.Length - 1);
            light_range = ColorPalette._ranges_bg_light[color_index];
            return ColorPalette._colors_bg[color_index];
    }    

    public static Color RandomOuterRingColor(ref int color_index)
    {
        color_index =
                Random.Range(0, ColorPalette._colors_ring.Length - 1);
            return ColorPalette._colors_ring[color_index];
    }

	public static Color GetBallInnerColor( int color_index )
	{
		if (color_index < 0 || color_index >= ColorPalette._colors_inner.Length) {
			return ColorPalette._colors_inner [0];
		}

		return ColorPalette._colors_inner[color_index];
	}

	public static Color GetBallPoisonColor( int color_index )
	{
		if (color_index < 0 || color_index >= ColorPalette._colors_poison.Length) {
			return ColorPalette._colors_poison [0];
		}

		return ColorPalette._colors_poison[color_index];
	}

	public static Color GetBallRingColor( int color_index )
	{
		if (color_index < 0 || color_index >= ColorPalette._colors_ring.Length) {
			return ColorPalette._colors_ring [0];
		}

		return ColorPalette._colors_ring[color_index];
	}

    public static Color RandomInnerFillColor(int color_index)
    {
		if (color_index >= ColorPalette._colors_inner.Length) {
			color_index = color_index % ColorPalette._colors_inner.Length;
		}
        return ColorPalette._colors_inner[color_index];
    }

    public static Color RandomPoisonFillColor(int color_index)
    {
		if (color_index >= ColorPalette._colors_poison.Length) {
			color_index = color_index % ColorPalette._colors_poison.Length;
		}
        return ColorPalette._colors_poison[color_index];
    }

    public static Color RandomFoodColor()
    {
        int color_index =
            Random.Range(0, ColorPalette._colors_food.Length - 1);
        return ColorPalette._colors_food[color_index];
    }
    
    public static Color RandomHedgeColor()
    {
        int color_index =
            Random.Range(0, ColorPalette._colors_hedge.Length - 1);
        return ColorPalette._colors_hedge[color_index];
    }
};
