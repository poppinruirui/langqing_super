﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObsceneMaskShader : MonoBehaviour {

	public GameObject _circle1;
	public GameObject _circle2;
	public GameObject _imaginarycircle;

	public SpriteRenderer _srCircle1;
	public SpriteRenderer _srCircle2;

	int m_nStatus = 0;
	float m_fOffsetX = 0.0f;
	float m_fOffsetY = 0.0f;

	float m_fTimeCount = 0.0f;

	void Awake()
	{
		_circle1 = this.gameObject.transform.Find ( "circle1" ).gameObject;
		_circle2 = this.gameObject.transform.Find ( "circle2" ).gameObject;
		_imaginarycircle = this.gameObject.transform.Find ( "imaginarycircle" ).gameObject;

		_srCircle1 = _circle1.GetComponent<SpriteRenderer> ();
		_srCircle2 = _circle2.GetComponent<SpriteRenderer> ();
	}

	// Use this for initialization
	void Start ()
	{
		this.gameObject.SetActive ( false );
	}
	
	// Update is called once per frame
	void Update () {
		Process ();
	}

	public void SetOffset( float fOffsetX, float fOffsetY )
	{
		m_fOffsetX = fOffsetX;
		m_fOffsetY = fOffsetY;
	}

	public void SetStatus( int nStatus )
	{
		m_nStatus = nStatus;
		m_fTimeCount = 0.0f;
	}

	public void Begin( float fDuration )
	{
		SetStatus (1);
		SetOffset ( 0.0f, -0.5f );
		_srCircle1.material.SetTextureOffset ("_Mask", new Vector2 (0.0f, -0.5f ));
		_srCircle2.material.SetTextureOffset ("_Mask", new Vector2 (0.0f, 0.5f ));
		this.gameObject.SetActive ( true );
		m_fTimeCount = 0.0f;
	}

	public void End()
	{
		SetStatus (3);
		this.gameObject.SetActive ( false );
	}

	void Process()
	{
		if (m_nStatus == 1) {
			_srCircle1.material.SetTextureOffset ("_Mask", new Vector2 (m_fOffsetX, m_fOffsetY));
			//m_fOffsetY += Time.deltaTime * 1.2f;
			m_fTimeCount += Time.deltaTime;
			m_fOffsetY = -0.5f + m_fTimeCount / Main.s_Instance.m_fMainTriggerPreUnfoldTime * 1.0f;
			if (m_fOffsetY >= 0.5f) {
				m_fOffsetY = 0.5f;
				SetStatus (2);
			}
		} else if (m_nStatus == 2) {
			_srCircle2.material.SetTextureOffset ("_Mask", new Vector2 (m_fOffsetX, m_fOffsetY));
			//m_fOffsetY -= Time.deltaTime * 1.2f;
			m_fTimeCount += Time.deltaTime;
			m_fOffsetY = 0.5f - m_fTimeCount / Main.s_Instance.m_fMainTriggerPreUnfoldTime * 1.0f;
			if (m_fOffsetY <= -0.5f) {
				m_fOffsetY = -0.5f;
				End ();
			}
		}
	}

	public bool IsEnd()
	{
		return m_nStatus > 2;
	}













}
