﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceManager : MonoBehaviour {

	public static ResourceManager s_Instance = null;

	void Awake()
	{
		s_Instance = this;
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public GameObject PrefabInstantiate( string szPrefabName )
	{
		return GameObject.Instantiate((GameObject)Resources.Load(szPrefabName));
	}

	public void RemoveTattooFood( TattooFood food )
	{
		GameObject.Destroy ( food.gameObject );
	}
}
