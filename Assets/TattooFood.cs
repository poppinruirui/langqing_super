﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TattooFood : MapObj {

    public enum eTattooFoodType
    {
        TattooFood_BeiShuDouZi, // 加体积的豆子
    };
    public eTattooFoodType _type = 0;
    public int _subtype = 0;
   
	public object[] _params = new object[8];

	public CircleCollider2D _Trigger;
	int m_nGUID = 0;

	public SpriteRenderer _sr;

	void Awake()
	{
		m_eObjType = eMapObjType.tattoo_food;
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public object GetParam( int nIndex )
	{
		return _params [nIndex];
	}

	public void SetParam( int nIndex, object param )
	{
		_params [nIndex] = param;
	}

	public int GetGUID()
	{
		return m_nGUID;
	}

	public void SetGUID( int nGUID )
	{
		m_nGUID = nGUID;
	}

	public void SetColor( Color color )
	{
		_sr.color = color;
	}
}
