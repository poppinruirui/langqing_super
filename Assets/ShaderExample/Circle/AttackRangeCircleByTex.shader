﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Custom/AttackRangeCircleByTex" {
	Properties 
	{
		_MainTex("MainTex" , 2D) = "white"{}
		_Color ("Color", Color) = (1,1,1,1)
		_Width("RoundWidth", float) = 100
	}
	SubShader //此shader是根据UV坐标,需要是正方形面片
	{
		//Pass   //此Pass是为了防止unity动态合并网格导致无法正常显示的   , 不会动态合并2个pass的物体  ...但这里显示多个时是正常的所以屏掉
		//{
		//	ZTest Off
		//	ZWrite Off
		//	ColorMask 0 ////////////ColorMask GB 只显示R通道,即红色
		//}
		Pass 
		{
			Blend SrcAlpha OneMinusSrcAlpha//////透明
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			struct v2f 
			{
				float4 pos : SV_POSITION;
				float2 oPos : TEXCOORD1;
			};

			fixed4 _Color;
			float _Width;
			sampler2D _MainTex;
			float4 _MainTex_ST;
			v2f vert (appdata_base v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.oPos = TRANSFORM_TEX(v.texcoord,_MainTex );
				o.oPos = o.oPos - float2(0.5,0.5);
				return o;
			}
			fixed4 frag(v2f i) : COLOR
			{
				float dis = sqrt(i.oPos.x * i.oPos.x + i.oPos.y * i.oPos.y);
				float maxDistance = 0.5;
				if(dis>0.5)
				{
					discard;
				}
				else
				{
					float ringWorldRange = unity_ObjectToWorld[0][0];//是物体的缩放比例
					float minDistance = 0.5 - _Width/ringWorldRange;
					if(dis < minDistance)
					{
						discard;
					}
					_Color.a = (dis - minDistance)/(0.5 - minDistance);
				}
				return _Color;
			}
			ENDCG
		}
	}
	FallBack "Diffuse"
}
