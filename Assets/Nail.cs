﻿/*
 * 钉子一旦吐出来，其实就跟原玩家队伍没有什么关系了，成为跟豆子和刺差不多的场景物件
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nail  : Obj
{
	float m_fLiveTimeCount = 0.0f;

	// Use this for initialization
	void Start () {
		
	}

	void Awake()
	{
		Init ();
	}
	
	// Update is called once per frame
	public override void Update () {
		base.Update ();

		m_fLiveTimeCount -= Time.deltaTime;
		if (m_fLiveTimeCount <= 0.0f) {
			Die ();
		}
	}

	public override void Init()
	{
		base.Init ();

		m_fLiveTimeCount = Main.s_Instance.m_fNailLiveTime;
	}

	public void Die ()
	{
		this.gameObject.SetActive ( false );
		GameObject.Destroy ( this.gameObject );
	}
}